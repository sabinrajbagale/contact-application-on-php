<!DOCTYPE html>
<html>
  <head>
    <title>contact</title>
    <style>
      table th,td{
        text-align: left;
        border: 1 px;
        line-height: 20px;
      }
      #HTMLtoPDF a{
        float: right;
      }
    </style>
  </head>
  <body>
    <h2 align="center">CONTACT APPLICATION</h2></a>
   <div id="HTMLtoPDF">
      <!-- here we call the function that makes PDF -->
      <a href="#" onclick="HTMLtoPDF()">Download PDF</a>
      <button onclick="mywholepage()"  >print</button>
      <script type="text/javascript">
        function mywholepage(){
          window.print();
        }
      </script>
    <table align="center" width="85%" >
      
      <tr>
        <th>NO.</th>
        <th>Name</th>
        <th>Number</th>
        <th>Address</th>
        <th>Relation</th>
      </tr>
      <br>
      <?php
      include('dbcon.php');
      
      $qry="SELECT * FROM `admin` ";
      $run=mysqli_query($con,$qry);
      ?>
      
      <?php
      $count=0;
      while($data=mysqli_fetch_array($run))
      {
      $count++;
      ?>
      <tr align="center">
        <td><?php echo $count;   ?></td>
        <td><?php echo $data['name']; ?></td>
        <td><?php echo $data['number']; ?></td>
        <td><?php echo $data['address'];   ?></td>
        <td><?php echo $data['relation'];   ?></td>
        </tr>
        <?php
        }
        ?>

        <!-- these js files are used for making PDF -->
        <script src="js/jspdf.js"></script>
        <script src="js/jquery-2.1.3.js"></script>
        <script src="js/pdfFromHTML.js"></script>
      </table>
      </div>
      </body>
    </html>